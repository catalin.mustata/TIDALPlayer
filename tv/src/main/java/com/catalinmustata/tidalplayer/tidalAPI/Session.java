package com.catalinmustata.tidalplayer.tidalAPI;

import android.util.Log;

import com.catalinmustata.tidalplayer.utils.Deferred;
import com.catalinmustata.tidalplayer.utils.Promise;
import com.catalinmustata.tidalplayer.utils.PromiseResponseHandler;
import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by catalinmustata on 27/11/2016.
 */

public final class Session {
    private static Session instance = null;

    private static final String TAG = "TidalSession";

    // TIDAL API DETAILS
    private static final String BASE_URL = "https://api.tidalhifi.com/v1/";
    private static final String API_TOKEN = "wdgaB1CilGA-S_s2";

    private static AsyncHttpClient client = new AsyncHttpClient();

    private static String sessionId;
    private static Long userId;
    private static String countryCode;
    private static String authToken;

    // Deferred objects - needed as LoopJ uses handlers instead of promises :(

    private static Deferred deferredLogin;
    private static Deferred deferredFavorites;

    public static Promise logIn(String username, String password) {
        RequestParams params = new RequestParams();
        params.put("token", API_TOKEN);
        params.put("username", username);
        params.put("password", password);

        deferredLogin = new Deferred();

        post("login/username", params, new PromiseResponseHandler(deferredLogin, (res) -> {
            JSONObject response = (JSONObject)res;
            try {
                sessionId = response.getString("sessionId");
                userId = response.getLong("userId");
                countryCode = response.getString("countryCode");
            } catch (JSONException ex) {
                sessionId = null;
                userId = null;
                countryCode = null;
                deferredLogin.reject(new Error(ex.getMessage()));
            }
        }));

        return deferredLogin.getPromise();
    }

    public static Promise getUserFavoriteArtists() {

        deferredFavorites = new Deferred();

        if (sessionId == null || userId == null || countryCode == null) {
            deferredFavorites.reject(new Error("Trying to get favorites without a session or a user"));
            return deferredFavorites.getPromise();
        }

        RequestParams params = new RequestParams();
        params.put("sessionId", sessionId);
        params.put("countryCode", countryCode);

        String URL = "users/" + userId + "/favorites/artists";

        get(URL, params, new PromiseResponseHandler(deferredFavorites));

        return deferredFavorites.getPromise();
    }

    public static Boolean isLoggedIn() {
        return (sessionId != null);
    }

    public static Long getUserId() {
        return userId;
    }

    // REQUEST UTILS
    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
