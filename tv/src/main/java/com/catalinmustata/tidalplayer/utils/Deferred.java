package com.catalinmustata.tidalplayer.utils;

/**
 * Created by catalinmustata on 02/12/2016.
 */

public class Deferred {

    private Promise promise;

    public Deferred() {
        promise = new Promise();
    }

    public Promise getPromise() {
        return promise;
    };

    @SuppressWarnings("unused")
    public void resolve(Object output) {
       promise.resolve(output);
    }

    @SuppressWarnings("unused")
    public void reject(Error reason) {
        promise.reject(reason);
    }

}
