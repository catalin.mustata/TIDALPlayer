package com.catalinmustata.tidalplayer.utils;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by catalinmustata on 10/12/2016.
 *
 * Allows for adding promises to a JsonHttpResponseHandler which seamlessly migrates any request to
 * a more modern, promise based, model.
 *
 * Optionally, a closure can be supplied, that will be executed as well, if the request is successful.
 * This will allow a Service layer class to execute it's own code independently of the promise
 * (which should be handled by the Service method caller (be it another Service or, more commonly,
 * a Controller)
 */

public class PromiseResponseHandler extends JsonHttpResponseHandler {

    /*
     * Deferred object to be fulfilled when the request using this handler returns
     */
    private Deferred promise;

    /*
     * Closure to be executed when request returns successfully
     */
    private Closure<Object> closure;

    public PromiseResponseHandler(Deferred promise) {
        this.promise = promise;
    }

    public PromiseResponseHandler(Deferred promise, Closure<Object> closure) {
        this.promise = promise;
        this.closure = closure;
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        if (statusCode == 200) {
            if (closure != null) {
                closure.run(response);
            }

            if (promise != null) {
                promise.resolve(response);
            }
        } else {
            if (promise != null) {
                promise.reject(new Error("Request failed with status: " + statusCode));
            }
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
        if (promise != null) {
            promise.reject(new Error("Request failed with code: " + statusCode));
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String message, Throwable throwable) {
        if (promise != null) {
            promise.reject(new Error("Request failed with code: " + statusCode + " and message: " + message));
        }
    }

}
