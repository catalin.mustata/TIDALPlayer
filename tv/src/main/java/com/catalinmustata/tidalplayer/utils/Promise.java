package com.catalinmustata.tidalplayer.utils;

import android.util.Log;

/**
 * Created by catalinmustata on 02/12/2016.
 */

public class Promise {

    private static final String TAG = "Promise";

    private enum States {
        CREATED,
        RESOLVED,
        REJECTED
    }

    private States state;
    private Object value;

    private Closure onComplete = null;
    private Closure onFailure = null;

    protected Promise() {
        state = States.CREATED;
    }

    protected void resolve(Object value) {
        this.value = value;
        if (this.state != States.CREATED) {
            Log.e(TAG, "Invalid state transition (from " + this.state + " to " + States.RESOLVED + "). Silently ignoring");
            return;
        }
        this.state = States.RESOLVED;

        if (onComplete != null) {
            onComplete.run(value);
        }
    }

    protected void reject(Object error) {
        this.value = error;
        if (this.state != States.CREATED) {
            Log.e(TAG, "Invalid state transition (from " + this.state + " to " + States.REJECTED + "). Silently ignoring");
            return;
        }
        this.state = States.REJECTED;

        if (onFailure != null) {
            onFailure.run(error);
        }
    }

    public Promise then(Closure<Object> function) {
        if (this.state == States.RESOLVED) {
            function.run(value);
        } else {
            onComplete = function;
        }
        return this;
    }

    public void otherwise(Closure<Object> function) {
        if (this.state == States.REJECTED) {
            function.run(value);
        } else {
            onFailure = function;
        }
    }

}
