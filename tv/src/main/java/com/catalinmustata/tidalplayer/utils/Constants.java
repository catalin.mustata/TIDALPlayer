package com.catalinmustata.tidalplayer.utils;

/**
 * Created by catalinmustata on 02/12/2016.
 */

public final class Constants {

    public static final String ACCOUNT = "tidalLogin";
    public static final String TOKEN_TYPE = "username";
    public static final String SESSION_ID = "session";
    public static final String AUTH_TOKEN = "auth_token";

}
