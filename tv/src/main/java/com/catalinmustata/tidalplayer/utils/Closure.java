package com.catalinmustata.tidalplayer.utils;

/**
 * Created by catalinmustata on 02/12/2016.
 */

public interface Closure<P extends Object> {

    void run(P parameter);

}
