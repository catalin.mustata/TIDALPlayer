package com.catalinmustata.tidalplayer.utils;

/**
 * Created by catalinmustata on 30/11/2016.
 */

public interface CMCallback {

    void execute();

}
