package com.catalinmustata.tidalplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.catalinmustata.tidalplayer.tidalAPI.Session;
import com.catalinmustata.tidalplayer.utils.Constants;

/**
 * Created by catalinmustata on 30/11/2016.
 */

public class LoginActivity extends Activity {
    private static final String TAG = "Login";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void attemptLogin(View view) {

        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);

        String authToken = prefs.getString(Constants.AUTH_TOKEN, null);
        if (authToken != null) {
            attemptTokenLogin(authToken);
        } else {
            attemptUsernameLogin();
        }

    }

    private void attemptTokenLogin(String authToken) {




    }

    private void attemptUsernameLogin() {

        final String username = ((EditText)findViewById(R.id.username)).getText().toString();
        final String password = ((EditText)findViewById(R.id.password)).getText().toString();

        Session
                .logIn(username, password)
                .then(obj -> {
                    //TODO: save user credentials here
                    Intent openApp = new Intent(this, MainScreen.class);
                    startActivity(openApp);
                })
                .otherwise(err -> {
                    Log.e(TAG, ((Error)err).getMessage());
                    Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
                });
    }
}
